package Application;

import model.VideogameCenter;
import model.VideogameCenterImpl;
import controller.MainController;
import controller.MainControllerImpl;
import view.MainPanelImpl;

/**
 * @author Mattia Ferrari
 */

public class Launcher {

	public static void main(String[] args) {

		MainPanelImpl panel = new MainPanelImpl();

		VideogameCenter videogameCenter = new VideogameCenterImpl();

		MainController mainController = new MainControllerImpl(videogameCenter,
				panel);
		panel.addObserver(mainController);

	}

}
