package view;

import controller.SectionController;

/**
 * @author Mattia Ferrari
 */

public interface InsertSection {

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(SectionController controller);

}
