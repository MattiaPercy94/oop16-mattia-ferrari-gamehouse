package model;

import java.util.ArrayList;
import java.io.*;



/**
 * @author Mattia Ferrari
 */

public class VideogameCenterImpl implements VideogameCenter, Serializable {

	private static final long serialVersionUID = 1L;
	private final ArrayList<Section> section;

	public VideogameCenterImpl() {

		this.section = getListSectionFile();

	};

	public void addSection(Section section) {

		this.section.add(section);
		insertSectionFile();

	}

	public int getNumberSection() {

		return section.size();

	}

	public ArrayList<Section> getListSection() {

		return section;

	}

	public void deleteSection(Section section) {

		this.section.remove(section);

	}

	public void insertSectionFile() {

		try {

			FileOutputStream stream = new FileOutputStream("Section.dat");

			ObjectOutputStream osStream = new ObjectOutputStream(stream);

			osStream.writeObject(section);

			osStream.flush();

			osStream.close();

		} catch (Exception e) {

			System.out.println("I/O errore");

		}
	}

	public ArrayList<Section> getListSectionFile() {

		try {

			FileInputStream stream = new FileInputStream("Section.dat");

			ObjectInputStream osStream = new ObjectInputStream(stream);

			@SuppressWarnings("unchecked")
			ArrayList<Section> sectionFile = (ArrayList<Section>) osStream.readObject();

			osStream.close();

			return sectionFile;

		} catch (Exception e) {

			System.out.println("I/O errore di stampa");

		}

		ArrayList<Section> arrayEmpty = new ArrayList<Section>();
		return arrayEmpty;

	}

	public boolean logIn(String username, String password) {

		try{
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("User.txt")));
			
			String line;
			while((line = bufferRead.readLine()) != null){
				String[] m = line.split(" ");
				
				for(int i = 0; i < m.length; i++){
					if(username.equals(m[i]) && password.equals(m[i+1])){
						return true;
					}
				}
			}
		}
		catch(Exception e){
			
		}
		return false;
	}

}
