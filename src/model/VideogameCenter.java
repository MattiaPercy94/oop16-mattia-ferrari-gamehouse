package model;

import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 */

public interface VideogameCenter {

	/**
	 * this method add a new Section in the VideogameCenter
	 * 
	 * @param section
	 */
	public void addSection(Section section);

	/**
	 * this method return the number of Section in VideogameCenter
	 * 
	 * @return
	 */
	public int getNumberSection();

	/**
	 * this method return the list of Section in VideogameCenter
	 * 
	 * @return
	 */
	public ArrayList<Section> getListSection();

	/**
	 * this method delete a Section in VideogameCenter
	 * 
	 * @param section
	 */
	public void deleteSection(Section section);

	/**
	 * this method return the file with Section in VideogameCenter
	 * 
	 * @return
	 */
	ArrayList<Section> getListSectionFile();

	/**
	 * this method works with the file of Section
	 */
	public void insertSectionFile();

	/**
	 * this method works with the file of Log-In
	 * 
	 * @param username
	 * @param password
	 * 
	 * @return boolean(String username, String password)
	 */
	public boolean logIn(String username, String password);

}
