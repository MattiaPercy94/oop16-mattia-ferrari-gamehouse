package model;

/**
 * @author Mattia Ferrari
 */

public interface Game {

	/**
	 * this method return the name of game
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * this method return the code of game
	 * 
	 * @return
	 */
	public int getCodeGame();

	/**
	 * this method return the quantity of game
	 * 
	 * @return
	 */
	public int getQuantity();

	/**
	 * this method return the price of game
	 * 
	 * @return
	 */
	public int getPrice();

	/**
	 * this method set a new name game
	 * 
	 * @param name
	 */
	public void setName(String name);

	/**
	 * this method set a new price game
	 * 
	 * @param price
	 */
	public void setPrice(int price);

	/**
	 * this method set a new quantity game
	 * 
	 * @param quantity
	 */
	public void setQuantity(int quantity);

	/**
	 * this method set a new code game
	 * 
	 * @param code
	 */
	public void setCode(int code);

}
