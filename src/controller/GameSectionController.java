package controller;

import model.Section;

import java.util.ArrayList;

import javax.swing.*;

/**
 * @author Mattia Ferrari
 */

public interface GameSectionController {

	/**
	 * this method delete a section
	 * 
	 * @param section
	 */
	void deleteSection(Section section);

	/**
	 * this method modify a section
	 * 
	 * @param section
	 * @param nameSection
	 */
	void setModifyController(Section section, JLabel nameSection);

	/**
	 * this method set the controller of modify section
	 * 
	 * @param section
	 */
	void setModifyGameController(Section section);
	
	/** 
	 * @return
	 */
	public ArrayList<Section> getListSectionView();

	/**
	 * exit
	 */
	void quit();

}
